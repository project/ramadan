<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
	<title><?php print $head_title ?></title>
</head>
<body>
	<div id="content">
		<div id="intro">
		<div id="logo">
	<h1><a href="<?php print $base_path ?>"><?php print $site_name ?></a></h1>
		</div>
		<ul id="menu">
<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array()) ?><?php } ?>
		</ul>

			<h1><?php print $site_slogan ?></h1>
			<p><?php print $subslogan ?></p>
			<div id="login">
<?php if (isset($secondary_links)) { ?><p><?php print theme('links', $secondary_links) ?></p><?php } ?>
<p align="right" color=white><?php echo date('l jS \of F Y '); echo date('h:i:s A');?></p>
	</div>	</div>	<?php if ($breadcrumb) { ?><div class="breadcrumb">
	<table><td valign="top">You are here:</td><td>
<?php print $breadcrumb; ?></td></table></div><?php } ?>
        <?php if ($sidebar_left) { ?>
		<div id="left">
        <?php print $sidebar_left ?>						
		</div>
        <?php } ?>
	
		<div id="right">
          <?php if ($mission) { ?><div class="mission"><?php print $mission ?></div><?php } ?>
          <?php if ($title) { ?><h2 class="pageTitle"><?php print $title ?></h2><?php } ?>
          <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
          <?php if ($help) { ?><div class="help"><?php print $help ?></div><?php } ?>
          <?php if ($messages) { ?><div class="messages"><?php print $messages ?></div><?php } ?>
	  <?php print $content_top; ?>
          <?php print $content; ?>
	  <?php print $content_bottom; ?>
	  <?php print $feed_icons; ?>
			<div style="clear: both"></div>
		</div>
			
		<div id="footer">
<div id="col1"><?php print $footer_message ?></div>
			<div id="col3">
		<p>Design By: Bisher Al-Halabi</p>
			</div>

		</div>	
	</div>
<?php print $closure ?>
</body>
</html>